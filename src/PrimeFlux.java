import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

class PrimeFlux {
    private static long counter = 21L;
    private static final int LIMIT = 100_003;
    private static final Long[] INITIAL_PRIME_LIST = new Long[]{2L, 3L, 5L, 7L, 11L, 13L, 17L, 19L};
    private static ArrayList<Long> primesList = new ArrayList<>(Arrays.asList(INITIAL_PRIME_LIST));

    PrimeFlux() {

    }

    private static void setPrimeNumbers() {
        int primeListSize = primesList.size();

        while (primeListSize < LIMIT) {
            double operation = counter / 2;

            Iterator<Long> primeNumbersList = primesList.iterator();
            primeNumbersList.next();

            while (primeNumbersList.hasNext()) {
                long primeNumber = primeNumbersList.next();

                if (primeNumber > operation) {
                    primesList.add(counter);
                    break;
                }

                boolean isNotPrime = counter % primeNumber == 0;
                if (isNotPrime) {
                    break;
                }
            }

            primeListSize = primesList.size();
            counter += 2;
        }
    }

    static ArrayList<Long> getPrimesList() {
        setPrimeNumbers();
        final int START = LIMIT - 1_001;
        final int END = primesList.size() - 1;
        ArrayList<Long> newPrimeList = new ArrayList(primesList.subList(START, END));
        return newPrimeList;
    }
}
