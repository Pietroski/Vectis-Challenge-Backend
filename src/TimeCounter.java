import java.time.Duration;
import java.time.Instant;

public class TimeCounter {
    private static Instant start;
    private static Instant end;
    private static Duration timeElapsed;
    private static TimeScale timeScale;

    TimeCounter(TimeScale timeScale) {
        this.timeScale = timeScale;
    }

    long defineTimeScale(Duration timeElapsed, TimeScale timeScale) {
        switch (timeScale) {
            case HOURS:
                return timeElapsed.toHours();
            case MINUTES:
                return timeElapsed.toMinutes();
            case SECONDS:
                return timeElapsed.toSeconds();
            case NANOSECONDS:
                return timeElapsed.toNanos();
            case MILLISECONDS:
            default:
                return timeElapsed.toMillis();
        }
    }

    void start() {
        start = Instant.now();
    }

    void end() {
        end = Instant.now();
    }

    long getTime() {
        timeElapsed = Duration.between(start, end);
        long scaledTimeElapsed = defineTimeScale(timeElapsed, timeScale);
        System.out.println(
                "This code execution took -> " + scaledTimeElapsed + " " + timeScale.toString()
        );
        return scaledTimeElapsed;
    }
}
