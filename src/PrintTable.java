import java.util.HashMap;
import java.util.Iterator;

class PrintTable {
    private HashMap<Integer, HashMap<Integer, Integer>> matrix;
    private int rowNumber = 0;
    private int columnNumber = 0;
    private HashMap<Integer, Integer> columnCellSizes = new HashMap<>();
    private int maxColumnSize = 0;

    PrintTable(HashMap<Integer, HashMap<Integer, Integer>> matrix) {
        this.matrix = matrix;
        setRowNumber();
        setColumnNumber();
        setColumnCellSizes();
        setMaxColumnCellSize();
    }

    private void setColumnNumber() {
        Iterator<HashMap<Integer, Integer>> matrixRows = matrix.values().iterator();
        while (matrixRows.hasNext()) {
            HashMap<Integer, Integer> matrixRow = matrixRows.next();
            int matrixRowSize = matrixRow.size();

            if (matrixRowSize > columnNumber) {
                columnNumber = matrixRowSize;
            }
        }
    }

    private void setRowNumber() {
        rowNumber = matrix.size();
    }

    private void setColumnCellSizes() {
        for (int i = 0; i < columnNumber; i++) {
            int columnCellSize = 0;

            for (int j = 1; j <= rowNumber; j++) {
                int column = matrix.get(j).get(i);
                int columnSize = Integer.toString(column).length() + 2;

                if (columnSize > columnCellSize) {
                    columnCellSize = columnSize;
                }
            }
            int index = i;
            int indexValue = columnCellSize;

            columnCellSizes.put(index, indexValue);
        }
    }

    private void setMaxColumnCellSize() {
        Iterator<Integer> sizeList = columnCellSizes.values().iterator();
        while (sizeList.hasNext()) {
            int number = sizeList.next();
            // int digitsNumberSize = Integer.toString(number).length();
            maxColumnSize += number;
        }
        maxColumnSize += columnNumber + 1 + 2;
    }

    private String setColumnsInRow (int number, int index) {
        int numberSize = Integer.toString(number).length();
        int cellSize = columnCellSizes.get(index);
        int cellSpace = cellSize - numberSize - 2;
        String columnsInRow = " " + " ".repeat(cellSpace) + number + " |";

        return columnsInRow;
    }

    private String getTableLineBreaker (String stringToConcatenate) {
        stringToConcatenate += "\n";
        stringToConcatenate += "-".repeat(maxColumnSize) + "\n";

        return stringToConcatenate;
    }

    void draw() {
        String tableString = "-".repeat(maxColumnSize) + "\n";

        int xIndex = 0, xSize = columnNumber;
        int yIndex = 0, ySize = rowNumber;

        while (yIndex <= ySize) {
            String tableRow = yIndex == 0 ? "X |" : yIndex + " |";

            if (yIndex == 0) {
                while (xIndex < xSize) {
                    int number = xIndex;
                    tableRow += setColumnsInRow(number, xIndex);

                    xIndex++;
                }
            } else {
                while (xIndex < xSize) {
                    int number = matrix.get(yIndex).get(xIndex);
                    tableRow += setColumnsInRow(number, xIndex);

                    xIndex++;
                }
            }

            tableString += getTableLineBreaker(tableRow);

            xIndex = 0;
            yIndex++;
        }

        System.out.println(tableString);
    }
}
