import java.util.HashMap;
import java.util.ArrayList;
import java.util.Iterator;

public class PrimeFluxCounter {
    private static ArrayList<Long> primesList;
    private static HashMap<Integer, HashMap<Integer, Integer>> positionRepetitionCounter = new HashMap<>();

    PrimeFluxCounter(ArrayList<Long> primesList) {
        this.primesList = primesList;
    }

    int getLargestPrimeNumberLength (long primeNumber) {
        String stringifiedPrimeNumber = Long.toString(primeNumber);
        return stringifiedPrimeNumber.length();
    }

    void setTableDimensions (int firstDimensionLength) {

        for (int index = 1;index <= firstDimensionLength;index++) {
            HashMap<Integer, Integer> digitsCounter = new HashMap<Integer, Integer>();
            for (int i = 0; i < 10; i++) {
                digitsCounter.put(i, 0);
            }
            positionRepetitionCounter.put(index, digitsCounter);
        }

        // System.out.println(positionRepetitionCounter);
    }

    void digitCounter (long numberToCount) {
        String strigifiedNumber = Long.toString(numberToCount);
        int stringifiedNumberLength = strigifiedNumber.length();
        int index = stringifiedNumberLength - 1;

        for (int position = 1; index >= 0; index --, position++) {
            int digit = Integer.parseInt(String.valueOf(strigifiedNumber.charAt(index)));
            int frequencyToUpdate = positionRepetitionCounter.get(position).get(digit);
            frequencyToUpdate++;
            positionRepetitionCounter.get(position).put(digit, frequencyToUpdate);
        }

    }

    void numberCounter () {
        Iterator<Long> primeNumbers = primesList.iterator();
        while (primeNumbers.hasNext()) {
            long primeNumber = primeNumbers.next();
            digitCounter(primeNumber);
        }
        System.out.println(positionRepetitionCounter);
    }

    void PrimeFrequencyCounter () {
        int primesListSize = primesList.size();
        long lastPrimeNumberFromPrimesList = primesList.get(primesListSize - 1);
        int largestPrimeNumberLength = getLargestPrimeNumberLength(lastPrimeNumberFromPrimesList);
        setTableDimensions(largestPrimeNumberLength);

        numberCounter();
    }

    public static void main(String[] args) {
        TimeCounter counter = new TimeCounter(TimeScale.SECONDS);
        counter.start();

        ArrayList<Long> primesList = PrimeFlux.getPrimesList();
        PrimeFluxCounter primeFrequencyCounter = new PrimeFluxCounter(primesList);
        primeFrequencyCounter.PrimeFrequencyCounter();

        counter.end();
        counter.getTime();

        PrintTable table = new PrintTable(positionRepetitionCounter);
        table.draw();
    }
}
