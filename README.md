# Vectis Challenge

This is a simple code answer to the challenge proposed by [Vectis](https://vectis.ai).

The Java version used to produce, run and test the code was the 15.0.2; so please, bear that in mind before running it.

## Challenge considerations

The average execution time of my algorithm is about 27-29 seconds. I could even have saved some milliseconds, but I decided not to do that due to code readability and keeping a function of a higher order.

Also, it is not ideal, but I chose to use string concatenation rather than a StringBuilder. For a small table as this was there is not much loss of performance. For the longer strings, however, it is most suitable to use StringBuilder rather than StringBuffer or string concatenation.

My front-end expertise is mainly in React with Redux and TypeScript. However, I always wanted to become a Java Engineering/Developer. For that reason I chose to tackle the back-end challenge although my back-end expertise is currently settled in Python with Flask and Django. I am currently doing some back-end maintenance and features on top of Node and Go. 

For that reason I explained right above, I decided to try the Java Challenge. On the weekend I got myself a little more familiarized with the Apache Bean and Apache Flink, but yesterday, on Monday, I decided to do all the data treatment and printing by hand. I sincerely hope you do not mind at all, and that you can see that I can quickly learn new things if needed and even quicker with good guidance. 

I like creating data treatment algorithms by hand but also I do not mind using libraries. In case it is a personal project I would rather do all by myself to practise. When, however, in a company, for example, as a faster and more robust solution is needed, I can think of using a library to have the job done.

As part of me, in my free time, I like bash scripting. As a side and own project I am currently working in a script that automatizes my environment initialization and configuration every time I have my computer formatted. It can also be used to automate other environment initializations such as Cloud VMs.

I have done some simple AWS deploy and have already used Oracle Cloud Solutions. Moving towards Google's would not be difficult at all. In the end, I am a linux/unix based system lover.

Here down below follows a simple handed-build solution example for the code challenge. The table was dynamically generated with its own PrintTable Class I built. I really liked the appearance of it and I sincerely hope you do like it as well.

### Output example
![alt text](./assets/Example_001.png)

# About me...
Please, check my curriculum... -> [Augusto Pietroski - Curriculum](./assets/EnglishCurriculum.pdf)

This repo is on -> [Vectis Backend Challenge Repo](https://gitlab.com/Pietroski/Vectis-Challenge-Backend).

Please, any doubts consider contacting me via e-mail at AugustoPTSantos@gmail.com

Yours sincerely,

Augusto Pietroski